/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package send_and_receive;

import java.net.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author carlo
 */
public class send {

    DatagramSocket aSocket = null;

    public boolean enviar(String args[]) {
        try {
            aSocket = new DatagramSocket();
            byte[] m = args[0].getBytes();
            InetAddress host = InetAddress.getByName(args[1]);
            int porta = 6789;
            DatagramPacket requisito = new DatagramPacket(m, m.length, host, porta);
            aSocket.send(requisito);
            byte[] buffer = new byte[1000];
            DatagramPacket reply = new DatagramPacket(buffer, buffer.length);
            aSocket.receive(reply);
            System.out.println("Reply: " + new String(reply.getData()));
        } catch (SocketException ex) {
            Logger.getLogger(send.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(send.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (aSocket != null)
            {
                aSocket.close();
            }
        }

        return true;
    }

}
