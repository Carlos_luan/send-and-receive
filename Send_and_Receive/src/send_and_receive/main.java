/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package send_and_receive;

import java.io.IOException;

/**
 *
 * @author carlo
 */
public class main {

    /**
     * @param args the command line arguments
     */
    public void initThread1() throws IOException {
        // Create a Runnable
        Runnable task = new Runnable() {
            public void run() {
                send enviar = new send();
            }
        };

        Thread backgroundThread = new Thread(task);

        backgroundThread.setDaemon(true);

        backgroundThread.start();
    }
public void  initThread2() throws IOException {
        // Create a Runnable
        Runnable task = new Runnable() {
            public void run() {
                receive receber = new receive();
                receber.receber();
            }
        };

        Thread backgroundThread = new Thread(task);

        backgroundThread.setDaemon(true);

        backgroundThread.start();
    }
    public static void main(String[] args)  
    {
        initThread2();
        initThread1();
    }
}
